function Review(author, date, comment, rating){
    this.ID = Date.now().toString();
    this.date = new Date(date);
    this.author = author;
    this.comment = comment;
    this.rating = [];
    this.rating = rating;
}

//First review data
var author = 'Игорь';
var dateReview = '2018-11-06 02:16:56';
var comment = 'Хороший продукт!';
var rating = [];
rating['service'] = 5;
rating['price'] = 4;
rating['value'] = 3;
rating['quality'] = 2;

//Second rewiew data
var author2 = 'Гена';
var dateReview2 = '2018-10-04 11:13:00';
var comment2 = 'Плохой продукт!';
var rating2 = [];
rating2['service'] = 2;
rating2['price'] = 3;
rating2['value'] = 4;
rating2['quality'] = 5;

//Third rewiew data
var author3 = 'Сергей';
var dateReview3 = '2019-10-04 11:13:00';
var comment3 = 'Нормальный продукт!';
var rating3 = [];
rating3['service'] = 3;
rating3['price'] = 3;
rating3['value'] = 3;
rating3['quality'] = 3;


var review1 = new Review(author, dateReview, comment, rating);
var review2 = new Review(author2, dateReview2, comment2, rating2);
var review3 = new Review(author3, dateReview3, comment3, rating3);

/*--------------------------------------реализация абстрактного класса---------------------------------- */
 
function AbstractProduct(name, description, price, images){
    if(this.constructor == AbstractProduct){
        throw new Error('Нельзя создавать экземпляры абстрактного класса!')
    }
    this.ID = Date.now().toString();
    this.name = name;
    this.description = description;
    this.price = price;
    this.images = images;
}
AbstractProduct.prototype.getID = function(){
    return this.ID;
};

AbstractProduct.prototype.getName = function(){
    return this.name;
};
AbstractProduct.prototype.setName = function(name){
    if(typeof name === 'string'){
        this.name = name;
        return this.name;
    }
    return false;
};

AbstractProduct.prototype.getDescription = function(){
    return this.description;
};
AbstractProduct.prototype.setDescription = function(description){
    if(typeof description === 'string'){
        this.description = description;
        return this.description;
    }
    return false;
};

AbstractProduct.prototype.getPrice = function(){
    return this.price;
};
AbstractProduct.prototype.setPrice = function(price){
    if(typeof price === 'number'){
        this.price = price;
        return this.price;
    }
    return false;
};

AbstractProduct.prototype.getImages = function(){
    return this.images;
};
AbstractProduct.prototype.getImage = function(name){
    if(!name){
        return this.images[0];
    }else{
        var index = this.images.indexOf(name);
        return index >= 0 ? this.images[index] : false;
    }
};

AbstractProduct.prototype.getFullInformation = function(){
    var originalObject = this;
    var fullInfo = "";
    var fullInfoArr = Object.keys(this);
    fullInfoArr.forEach(function(key){
        fullInfo += key + ' - ' + originalObject[key] + "\n";
    });
    return fullInfo;
};

AbstractProduct.prototype.getPriceForQuantity = function(int){
    return '$' + this.price * int;
};

AbstractProduct.prototype.doAnyAction = function(property, param){
    var actionType = null;
    var methodName = null;
    if(arguments.length == 1){
        actionType = 'get';
        methodName = actionType + arguments[0];
        return this[methodName]();
    }else if(arguments.length == 2){
        actionType = 'set';
        methodName = actionType + arguments[0];
        console.log(methodName);
        return this[methodName](arguments[1]);
    }
};

Object.defineProperty(AbstractProduct.prototype, 'getProductTileHTML',{
    value: function(){
        //Product overlay
        var productOverlay = document.createElement('div');
        productOverlay.classList.add('product-overlay');
        
        //Ссылка с изображением:
        var imagelink = document.createElement('a');
        imagelink.href = "#";
        imagelink.innerHTML = "<img src='" + this.images[0] + "' alt='ProductImage'>";

        productOverlay.append(imagelink);

        /*-------------Формируем div.img-overlay-------------*/
        var imgOverlay = document.createElement('div');
        imgOverlay.classList.add('img-overlay');

        //Product link
        var productLink = document.createElement('a');
        productLink.href = "#";
        productLink.title = this.name;

        //Img-overlay-area
        var imgOverlayArea = document.createElement('div');
        imgOverlayArea.classList.add('img-overlay-area');
        
        //Верхняя цена товара
        var h2Name = "<h2 class='heading'>" + this.name +"</h2>";
        var h2Name = document.createElement('h2');
        h2Name.classList.add('heading');
        var name = document.createTextNode(this.name); 
        h2Name.append(name);

        //heading-holder
        var headingHolder = document.createElement('div');
        headingHolder.innerHTML = '<strong class="price"><div class="price-box"><span class="regular-price"><price class="price">&#8364;' + this.price + '</price></span></div></strong>';

        //Holder
        var holder = document.createElement('div');
        holder.classList.add('holder');
        holder.innerHTML = "<a href='#' class='quickview' rel='nofollow'>" + this.description + "</a>";


        imgOverlayArea.append(h2Name);
        imgOverlayArea.append(headingHolder);
        productLink.append(imgOverlayArea);
        imgOverlay.append(productLink);
        imgOverlay.append(holder);

        /*-------------Окончание формирования div.img-overlay-------------*/

        productOverlay.append(imgOverlay);

        //нижнее имя товара:
        var nameDown = document.createElement('h3');
        nameDown.innerHTML = "<a href='#'>" + this.name + "</a>";
        nameDown.classList.add('product-name');

        //нижняя цена товара:
        var priceDown = document.createElement('div');
        priceDown.classList.add('price-box');
        priceDown.innerHTML = "<span class='regular-price'><span class='price'>&#8364;" + this.price + "</span></span>";

        /*-------------Формируем тайл продукта--------------*/

        var newTile = document.createElement('li');
        newTile.classList.add('col-3');
        newTile.append(productOverlay);
        newTile.append(nameDown);
        newTile.append(priceDown);

        /*-------------Окончание формирования тайла продукта--------------*/

        var goodsList = document.getElementById('product-grid');
        //console.log(goodsList);
        goodsList.append(newTile);
    }
})

/*--------------------------------------реализация дочернего класса Clothes---------------------------------- */

function Clothes(params){
    AbstractProduct.call(this, 
                         params.name, 
                         params.description, 
                         params.price, 
                         params.images
                        );
    this.brand = params.brand;
    this.sizes = params.sizes;
    this.activeSize = params.activeSize;
    this.quantity = params.quantity;
    this.date = new Date(params.date);
    this.reviews = params.reviews;
}
Clothes.prototype = Object.create(AbstractProduct.prototype);
Clothes.prototype.constructor = Clothes;

Clothes.prototype.getBrand = function(){
    return this.brand;
};
Clothes.prototype.setBrand = function(brand){
    if(typeof brand === 'string'){
        this.brand = brand;
        return this.brand;
    }
    return false;
};

Clothes.prototype.getSizes = function(){
    return this.sizes;
};
Clothes.prototype.addSize = function(size){
    this.sizes.push(size);
    return this.sizes;
};

Clothes.prototype.deleteSize = function(size){
    if(!size){
        return 'Пожалуйста, введите размер для удаления!';
    }else{
        var index = this.sizes.indexOf(size);
        if(index >= 0){
            this.sizes.splice(index, 1);
            return this.sizes;
        }
        return 'Введенного размера не существует!';
    }
};

Clothes.prototype.getActiveSize = function(){
    return this.activeSize;
};
Clothes.prototype.setActiveSize = function(size){
    if(!size){
        return this.activeSize;
    }
    var index = this.sizes.indexOf(size);
    if(index >= 0){
        this.activeSize = this.sizes[index];
    }
    return this.activeSize;
};

Clothes.prototype.getQuantity = function(){
    return this.quantity;
};

Clothes.prototype.getDate = function(){
    return this.date;
};
Clothes.prototype.setDate = function(date){
    var newDate = new Date(date);
    if(newDate == 'Invalid Date'){
        return false;
    }
    this.date = newDate;
    return this.date;
};

Clothes.prototype.getReviews = function(){
    return this.reviews;
};
Clothes.prototype.addReview = function(review){
    if(!review){
        return 'Добавьте отзыв!';
    }
    if(typeof review != 'object'){
        return 'неправильный формат данных!';
    }
    this.reviews.push(review);
    return this.reviews;
};
Clothes.prototype.deleteReview = function(id){
    var index = this.reviews.findIndex(function(item){
                    return item.ID === id;
                });
    this.reviews.splice(index, 1);
    return this.reviews;
};

Clothes.prototype.getAverageRating = function(){
    var averRate = 0;
    var subRate = 0;
    var counter = 0;
    this.reviews.forEach(function(item){
        for (key in item.rating){
            subRate += item.rating[key];
            counter++;
        }
        subRate = subRate/counter;
        averRate += subRate;
        counter = 0;
        subRate = 0;
    });
    averRate = averRate/this.reviews.length;
    return averRate;
};

Object.defineProperties(Clothes, {color:{}, material:{}}, {});

Object.defineProperty(Clothes.prototype, 'Color',{
    get: function(){
        return this.color;
    },
    set: function(color){
        console.log('colorCame', color);
        console.log('typeofColor', typeof color);
        if(typeof color === 'string'){
            this.color = color;
            return;
        }
        return false;
    }
});

Object.defineProperty(Clothes.prototype, 'Material',{
    get: function(){
        return this.material;
    },
    set: function(material){
        if(typeof material === 'string'){
            this.material = material;
            return;
        }
        return false;
    }
});

/*--------------------------------------Создание и проверка экземпляра Clothes---------------------------------- */

var clothesParams = {
    name: "Snickers",
    description: "Just an ordinary description",
    price: 555.5,
    brand: "Abibas",
    activeSize: 'XL',
    sizes: ['XS','S', 'M','L', 'XL', 'XXL'],
    quantity: 10,
    date: '2019-11-06 02:16:56',
    reviews: [review1],
    images: ['/public/img1.jpg', '/public/img2.jpg']
 };

 var snickers = new Clothes(clothesParams);

console.log('ID:', snickers.getID());
console.log('name:', snickers.getName());
console.log('description:', snickers.getDescription());
console.log('price:', snickers.getPrice());
console.log('brand:', snickers.getBrand());
console.log('sizes:', snickers.getSizes());
console.log('activeSize:', snickers.getActiveSize());
console.log('quantity:', snickers.getQuantity());
console.log('date:', snickers.getDate());
console.log('reviews:',snickers.getReviews());
console.log('images:', snickers.getImages());
console.log('particular image:', snickers.getImage('/public/img2.jpg'));
console.log('set color:', snickers.Color = "red");
console.log('get color:', snickers.Color);
console.log('set material:', snickers.Material = 'cotton');
console.log('get material:', snickers.Material);
console.log('particular image:', snickers.getImage('/public/img2.jpg'));
console.log('particular image:', snickers.getImage('/public/img2.jpg'));
console.log('Новое имя:', snickers.setName('Boots'));
console.log('Новое описание:', snickers.setDescription('Обновленное описание'));
console.log('Новая цена:', snickers.setPrice(120.80));
console.log('Новый бренд:', snickers.setBrand('Brand new brаnd'));
console.log('+ новый размер XLL:', snickers.addSize('XLL'));
console.log('удаляем размер XLL:', snickers.deleteSize('XLL'));
console.log('Выставляем активный размер в S:', snickers.setActiveSize('S'));
console.log('Выставляем новую дату:', snickers.setDate('2000-1-01 01:01:01'));
console.log('Добавляем новый отзыв:', snickers.addReview(review2));
console.log('Высчитать цену:', snickers.getPriceForQuantity(10));
console.log('Полная информация о товаре:\n', snickers.getFullInformation());
//console.log('Удаляем отзыв:', product.deleteReview(review2.ID));
console.log('Получаем общую оценку:', snickers.getAverageRating());
console.log('Универсальный метод:', snickers.doAnyAction('Brand', 'Rebook'));
console.log('Универсальный метод:', snickers.doAnyAction('Price', '666.66'));
snickers.getProductTileHTML();

/*--------------------------------------реализация дочернего класса Electronics---------------------------------- */

function Electronics(params){
    AbstractProduct.call(this, 
        params.name, 
        params.description, 
        params.price, 
        params.images
    );
    this.power = params.power;
    this.warranty = params.warranty;
}
Electronics.prototype = Object.create(AbstractProduct.prototype);
Electronics.prototype.constructor = Electronics;

Object.defineProperties(Electronics, {power:{}, warrany:{}}, {});

Object.defineProperty(Electronics.prototype, 'Power',{
    get: function(){
        return this.power;
    },
    set: function(power){
        if(typeof power === 'number'){
            this.power = power;
            return this.power 
        }
        return false;
    }
});

Object.defineProperty(Electronics.prototype, 'Warranty',{
    get: function(){
        return this.warranty;
    },
    set: function(warranty){
        if(typeof warranty === 'number'){
            if(warranty > 0 && warranty < 10){
                this.warranty = warranty;
                return this.warranty
            }
        }
        return false;
    }
});


/*--------------------------------------Создание и проверка экземпляра Electronics---------------------------------- */

var electronicParams = {
    name: "Лампа",
    description: "Just an ordinary desc",
    price: 555.5,
    images: ['/public/img1.jpg', '/public/img2.jpg'],
 };
 var electronicParams2 = {
    name: "Холодильник",
    description: "Some other description",
    price: 130.2,
    images: ['/public/img1.jpg', '/public/img2.jpg'],
 };
var electricLamp = new Electronics(electronicParams);
var refrigirator = new Electronics(electronicParams2);


console.log('ID:', electricLamp.getID());
console.log('name:', electricLamp.getName());
console.log('description:', electricLamp.getDescription());
console.log('price:', electricLamp.getPrice());
console.log('imageList:', electricLamp.getImages());
console.log('images:', electricLamp.getImage('/public/img2.jpg'));
console.log('set warranty:', electricLamp.Warranty = 9);
console.log('get warranty:', electricLamp.Warranty);
console.log('set power:', electricLamp.Power = 220);
console.log('get power:', electricLamp.Power);
console.log('Новое имя:', electricLamp.setName('лампа накаливания'));
console.log('Новое описание:', electricLamp.setDescription('Updated description'));
console.log('Новая цена:', electricLamp.setPrice(120.80));
console.log('Высчитать цену:', electricLamp.getPriceForQuantity(10));
console.log('Полная информация о товаре:\n', electricLamp.getFullInformation());
console.log('Универсальный метод:', electricLamp.doAnyAction('Name', 'Superlamp'));
console.log('Универсальный метод:', electricLamp.doAnyAction('Price', 666.66));
electricLamp.getProductTileHTML();
/*--------------------------------------Создание и проверка функций выборки и сортировки---------------------------------- */

var products = [];
products.push(electricLamp, refrigirator);

function SearchProducts(products, searchText){
    var newArr = [];
    products.forEach(function(product){
        var ifExist = product.name.indexOf(searchText);
        if(ifExist !== -1){
            newArr.push(product);
        }else{
            var ifExist = product.description.indexOf(searchText);
            if(ifExist !== -1){
                newArr.push(product);
            }
        }
    });
    return newArr;
}

function Sort(products, sortRule){
    products.sort(function(a, b){
        if(typeof a[sortRule] == 'string' || typeof b[sortRule] == 'string'){
            if(a[sortRule].toLowerCase() > b[sortRule].toLowerCase()) return 1;
            if(a[sortRule].toLowerCase() == b[sortRule].toLowerCase()) return 0;
            if(a[sortRule].toLowerCase() < b[sortRule].toLowerCase()) return -1;
        }
        if(a[sortRule] > b[sortRule]) return 1;
        if(a[sortRule] == b[sortRule]) return 0;
        if(a[sortRule] < b[sortRule]) return -1;
    });

}
var matchingProducts = SearchProducts(products, 'des');
console.log('Подходящие объекты', matchingProducts);
Sort(products, 'warranty');
console.log('Отсортированные объекты', products );

/*--------------------------------------Объект для валидации полей ---------------------------------- */

function Validator(){
    this.validateEmail = function(email){
        var emailRegExpr = /^[A-Za-z0-9]([A-Za-z0-9-.]){1,19}@[\w.!$%&'*+/=?^_-]{1,15}\.[A-Za-z]{1,5}$/
        console.log('Is email valid:', emailRegExpr.test(email));
    };
    this.validatePhone = function(phone){
        var phoneRegExpr = /(?=(^[\d-\s\+\(\)]{10,25}$))^[\s-]*?(\+[\s-]*?3[\s-]*?8[\s-]*?)?[\s-]*?\(?[\s-]*?0[\s-]*?9[\s-]*?9[\s-]*?\)?(?=(([^\d]*?\d[^\d]*?){7}$))[\d\s-]*?$/g
        console.log('Is phone valid:', phoneRegExpr.test(phone));
    };
    this.validatePassword = function(pass){
        var passRegExpr = /^[A-Z](?=((.*[A-Z].*){1,}$))\w{4,10}$/ 
        console.log('Is password valid:', passRegExpr.test(pass));
    };
}

var infoValidator = new Validator();

infoValidator.validateEmail('firstpart@.se.endde.ded');
infoValidator.validatePhone('+38 (099) 567 8901');
infoValidator.validatePassword('SupperPas1');